package pwndpass

import (
	"testing"
)

func TestPassword(t *testing.T) {

	valid, _ := CheckPassword("hello")

	if !valid {
		t.Error("hello is certainly invalid")
	}

	valid, err := CheckPassword("398rue98ruew8rwug98rwuuegjuoijfjfdspvfmvkcsodfr903mfsodc83qdkdiekdldld")

	if err != nil {
		t.Error(err)
	}
	if valid {
		t.Error("I doubt my mash exists")
	}

}
