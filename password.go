package pwndpass

import (
	"bufio"
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"net/http"
	"strings"
)

// CheckPassword checks given password if it's on pwnedpasswords using k-anonymity.
// This uses a standard http.Client and if you want to use your own you should use
// CheckPasswordClient
func CheckPassword(password string) (bool, error) {
	return CheckPasswordClient(password, &http.Client{})
}

// CheckPasswordClient checks given password if it's on pwnedpasswords using k-anonymity.
// This uses the provided http.Client which can be used inside Cloud Engine, for example.
func CheckPasswordClient(password string, client *http.Client) (bool, error) {
	shaSum := sha1.Sum([]byte(password))
	passwordHex := strings.ToUpper(hex.EncodeToString(shaSum[:]))
	resp, err := client.Get(fmt.Sprintf("https://api.pwnedpasswords.com/range/%s", passwordHex[:5]))
	if err != nil {
		return false, err
	}

	defer resp.Body.Close()

	scanner := bufio.NewScanner(resp.Body)
	for scanner.Scan() {
		text := scanner.Text()
		suffix := strings.Split(text, ":")[0]
		if suffix == passwordHex[5:] {
			return true, nil
		}
	}

	return false, nil
}
